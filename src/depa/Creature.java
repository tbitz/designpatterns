package depa;

abstract public class Creature {
	
	protected String name;
	protected int weight;
	protected String sound;
	
	// must be overwritten
	public abstract void setName(String newName);
	public abstract String getName();
	
	public abstract void setWeight(double newWeight);
	public abstract double getWeight();

}
