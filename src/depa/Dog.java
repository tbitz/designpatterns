package depa;

public class Dog extends Animal {
	
	public void digHole() {
		System.out.println("Dug a hole");
	}
	
	public Dog() {
		super();
		
		setSound("Bark");
	}
	
	public void changeVar(int randNum) {
		randNum = 12;
		System.out.println("Randnum in method: " + randNum);
	}
	
	private void bePrivate() {
		System.out.println("In private method");
	}
	
	public void accessPrivate() {
		System.out.println("In private method");
	}
	
	
}
