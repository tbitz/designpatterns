package depa;

public class WorkWithAnimals {
	
	static int justANum = 10;

	public static void main(String[] args) {
		Giraffe giraffe = new Giraffe();
		giraffe.setName("Frank");
		System.out.println(giraffe.getName());
		
		Dog fido = new Dog();
		fido.setName("fido");
		fido.setWeight(-1);
		System.out.println(fido.getName());
		fido.digHole();
		
		Cat catty = new Cat();
		catty.setWeight(100);
		
		int randNum = 10;
		fido.changeVar(randNum);
		System.out.print(randNum);
		run(fido); // reference to the object but still changes name
		System.out.println(fido.getName());
		
		Animal dog = new Dog();
		Animal cat = new Cat();
		System.out.print(dog.getSound());
		System.out.print(cat.getSound());
		speakAnimal(dog);
		
		((Dog)dog).digHole();
		System.out.println(justANum);
		
		fido.accessPrivate();
	}
	
	public static void run(Dog fido) {
		fido.setName("Marcus");
	}
	
	public static void speakAnimal(Animal randAnimal) {
		System.out.println("Animal says " + randAnimal.getSound());
	}

}
