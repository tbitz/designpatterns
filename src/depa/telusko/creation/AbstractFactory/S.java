package depa.telusko.creation.AbstractFactory;

public abstract class S {
	private String name;
	W weapon;
	E engine;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return "The " + name + " has a top speed of " + engine + " and an attack power of " + weapon;
	}
	
	public void followHeroShip(){
		
		System.out.println(getName() + " is following the hero at " + engine );
		
	}
	
	public void displayEnemyShip(){
		
		System.out.println(getName() + " is on the screen");
		
	}
	
	public void enemyShipShoots(){
		System.out.println(getName() + " attacks and does " + weapon);
	}
	
	abstract void makeS();
}
