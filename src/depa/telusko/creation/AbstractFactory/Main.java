package depa.telusko.creation.AbstractFactory;

public class Main {

	public static void main(String[] args) {
		SB sb = new UB();
		S s1 = sb.orderS("UFO1");
		System.out.println(s1.toString());
		
		System.out.println("-----------------");
		
		S s2 = sb.orderS("UFO2");
		System.out.println(s2.toString());
	}

}
