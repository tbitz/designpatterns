package depa.telusko.creation.AbstractFactory;

public class UB extends SB {

	@Override
	public S makeS(String typeofS) {
		S s = null;
		
		if(typeofS.equals("UFO1")) {
			SF partsfactory = new U1F();
			s = new U1(partsfactory);
			s.setName("CRAZYUFO1");
		}
		
		else if(typeofS.equals("UFO2")) {
			SF partsfactory = new U2F();
			s = new U2(partsfactory);
			s.setName("SUPERUFO2");
		}
		
		return s;
	}

}
