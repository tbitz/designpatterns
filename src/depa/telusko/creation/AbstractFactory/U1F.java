package depa.telusko.creation.AbstractFactory;

public class U1F implements SF {

	@Override
	public W addUGun() {
		return new UGun();
	}

	@Override
	public E addUEngine() {
		return new UEngine();
	}
	
}
