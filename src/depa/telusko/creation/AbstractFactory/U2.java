package depa.telusko.creation.AbstractFactory;

public class U2 extends S {
	
	SF sf;
	
	public U2(SF sf) {
		this.sf = sf;
	}

	@Override
	void makeS() {
		System.out.println("Making enemy ship " + getName());

		weapon = sf.addUGun();
		engine = sf.addUEngine();
	}

}
