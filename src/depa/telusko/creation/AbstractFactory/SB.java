package depa.telusko.creation.AbstractFactory;

public abstract class SB {

	public abstract S makeS(String typeofS);
	
	public S orderS(String typeofS) {
		S s = makeS(typeofS);
		
        s.makeS();
        s.displayEnemyShip();
        s.followHeroShip();
        s.enemyShipShoots();


		return s;
	}

}
