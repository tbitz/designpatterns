package depa.telusko.creation.AbstractFactory;

public class U1 extends S {
	
	SF sf;
	
	public U1(SF sf) {
		this.sf = sf;
	}

	@Override
	void makeS() {
		System.out.println("Making enemy ship " + getName());

		weapon = sf.addUGun();
		engine = sf.addUEngine();
	}

}
