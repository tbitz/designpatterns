package depa.telusko.creation;

public class SingletonEasy {
	
	public static void main(String[] args) {
		System.out.println(args);
		SingletonWay s1 = SingletonWay.INSTANCE;
		s1.name = "Thierry";
		SingletonWay s2 = SingletonWay.INSTANCE;
		s2.name = "Alain";
		System.out.println(s1.name); // it changes s1
	}

}

enum SingletonWay {
	INSTANCE;
	String name;
	
	public void show() {
		System.out.println(name);
	}
}
