package depa.telusko.creation;

public class Singleton {
	
	public static void main(String[] args) {
		SingletonEnum singleton1 = SingletonEnum.INSTANCE;
		singleton1.i = 5;
		singleton1.show();
		SingletonEnum singleton2 = SingletonEnum.INSTANCE;
		singleton2.i = 11;
		singleton1.show();
		
		
		
		Abc obj1 = Abc.getInstance();
		Abc obj2 = Abc.getInstance();
		
		obj1.setName("charlie");
		obj2.setName("rita"); // we are changing the name on the singleton		
		
		Thread t1 = new Thread(new Runnable() {			
			@Override
			public void run() {
				Single b1 = Single.getInstance3();
				b1.setName("bobby");
			}
		});
		
		Thread t2 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				Single b2 = Single.getInstance3();
				b2.setName("Lisa");
			}
		});
		
		t1.start();
		// try { Thread.sleep(10); } catch(Exception e) {};
		
		t2.start();

	}


}

enum SingletonEnum {
	INSTANCE; // We have created a singleton :D
	int i;
	public void show() {
		System.out.println(i);
	}
}

// lazy singleton creation, the way to go
class Single {
	public static Single obj; 
	
	private Single() {
		
	}
	
	// Synchronized to handle Threading problems
	public static synchronized Single getInstance() {
		if(obj == null) {
			obj = new Single();	
		}
		return obj;
	}
	
	// Double Checked Locking if Singleton is big
	public static Single getInstance2() { 
		if(obj == null) {
			System.out.println("created instance");
			obj = new Single();
		}
		return obj;
	}
	
	// Double Checked Locking if Singleton is big (synchronized way 2)
	public static Single getInstance3() { 
		if(obj == null) {
			// first time is rejected here
			synchronized(Abc.class) {
				if(obj == null) {
					obj = new Single();
					System.out.println("created instance");
					
				}
			}
				
			
		}
		return obj;
	}
	
	// enum way: less resources, better safety
	
	// additional
	private String name;
	public void setName(String setname) {
		name = setname;				
	}
	public String getName() {
		return name;
	}
	
	
}


// eager, bad version
class Abc {
	// create a static object in class
	// global variable, if bulky uses a lot of memory
	// will be created even if we are not using it
	public static Abc obj = new Abc(); 
	// make it lazy, not eager: create instance at the calling of getInstance
	
	// private to make creation impossible
	private Abc() {
		
	}
	
	// return a static object in this static method
	public static Abc getInstance() {
		return obj;
	}
	
	// additional
	private String name;
	public void setName(String setname) {
		name = setname;
	}
	public String getName() {
		return name;
	}
	
	
}
