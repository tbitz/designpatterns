package depa.telusko.creation.Factory;

public class FactoryMain {

	public static void main(String[] args) {
		OS obj = new Windows(); // providing the implementation hear
		obj.spec();
		
		OSFactory osfactory = new OSFactory();
		OS obj2 = osfactory.getInstance("Open");
		obj2.spec();
	}

}
