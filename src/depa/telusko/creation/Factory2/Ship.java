package depa.telusko.creation.Factory2;

public abstract class Ship {
	private String name;
	private int speed;
	private int damage;	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getDamage() {
		return damage;
	}
	public void setDamage(int damage) {
		this.damage = damage;
	}
	
	public void getData() {
		System.out.println("Name: " + getName() + ", Speed: " + getSpeed() + ", Damage: " + getDamage());
	}
	
	
}
