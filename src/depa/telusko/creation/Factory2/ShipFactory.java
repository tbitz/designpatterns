package depa.telusko.creation.Factory2;

public class ShipFactory {

	public Ship produce(String ship) {
		if(ship.equals("S")) {
			return new SmallUFO();
		}
		else if(ship.equals("B")) {
			return new BigUFO();
		}
		return null;
	}
	
	
	
}
