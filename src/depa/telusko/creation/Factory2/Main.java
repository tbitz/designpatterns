package depa.telusko.creation.Factory2;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		ShipFactory shipfactory = new ShipFactory();
		Ship ship = null;
		
		Scanner userinput = new Scanner(System.in);
		System.out.println("What type of ship: S / B");
		if(userinput.hasNextLine()) {
			String shiptype = userinput.nextLine();
			ship = shipfactory.produce(shiptype);
			ship.getData();
		} 
	}

}
